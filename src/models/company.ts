export interface Company {
    id: number;
    photo: string;
    description: string;
    country: string;
    enterprise_name: string;
}