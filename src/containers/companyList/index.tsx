import React from 'react'
import { useHistory } from 'react-router-dom'
import CompanyItem from '../../components/CompanyListItem'
import { Company } from '../../models/company'
import {
    Typography,
} from "@material-ui/core";
interface CompanyListProps {
   data: Company[]; 
}


const CompanyList : React.FC<CompanyListProps> = (props) => {
    const history = useHistory();
    if (props.data.length === 0) {
        return (
            <div style={{
                height: '80vh',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                width: '100%'
            }}>
                <Typography>Nenhuma empresa foi encontrada para busca</Typography>
            </div>
        )
    }

    return (
        <>
            {
                props.data.map(item => (
                    <div key={item.id} style={{ marginBottom: '20px'}}>
                        <CompanyItem  
                            onClick={() => history.push(`/companies/${item.id}`)}
                            brand={item.enterprise_name}
                            address={item.country}
                            description={item.description}
                            companyLogo={`https://empresas.ioasys.com.br/${item.photo}`}/> 
                    </div>
                ))
            }
        </>
    )
}

export default CompanyList
