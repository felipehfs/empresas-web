import React, { useState, useEffect } from "react";
import {
  AppBar,
  Box,
  createStyles,
  IconButton,
  makeStyles,
  Paper,
  Toolbar,
  Typography,
} from "@material-ui/core";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { useHistory, useParams } from "react-router-dom";
import { Company } from "../../models/company";
import api from "../../api";


const useStyles = makeStyles((theme) =>
  createStyles({
    container: {
      background: "#eeecdb",
      minHeight: "100vh",
      width: "100%",
    },
    navDefault: {
      display: "flex",
      alignItems: "center",
      width: "100%",
    },
    main: {
        paddingTop: 100,
        minHeight: "100vh",
        paddingRight: 16,
        paddingLeft: 16,
        display: 'flex',
      },
      card: {
          width: '100%',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          padding: theme.spacing(3)
      },
      logo: {
          maxHeight: 260,
          width: '100%',
          marginBottom: theme.spacing(4)
      },
  })
);

const CompanyDetails: React.FC = () => {
  const classes = useStyles();
  const history = useHistory()
  const params = useParams<{ id: string }>();
  const [company, setCompany] = useState<Company>({
    id: 0,
    country: '',
    description: '',
    enterprise_name: '',
    photo: ''
  });

  useEffect(() => {
    async function fetchCompany() {
      const sessionInfo = localStorage.getItem("session_info");
      if (sessionInfo !== null) {
        const session = JSON.parse(sessionInfo);
        api.defaults.headers.client = session.client;
        api.defaults.headers.uid = session.uid;
        api.defaults.headers["access-token"] = session["access-token"];
      }
      const response = await api.get(`enterprises/${params.id}`);
      setCompany(response.data.enterprise);
    }

    fetchCompany();
  }, []);

  return (
    <Box className={classes.container}>
      <AppBar>
        <Toolbar>
            <nav className={classes.navDefault}>
                <IconButton onClick={history.goBack}>
                    <ArrowBackIcon />
                </IconButton>
                <Typography>{company.enterprise_name}</Typography>
            </nav>
        </Toolbar>
      </AppBar>
        <Box className={classes.main}>
            <Paper className={classes.card}>
                <img 
                    className={classes.logo}
                src={`https://empresas.ioasys.com.br/${company.photo}`}
    alt="Logo"/>
                <Typography variant="body1">
                    {company.description}
                </Typography>
            </Paper>

        </Box>
    </Box>
  );
};

export default CompanyDetails;
