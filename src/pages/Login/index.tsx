import React, { useState} from "react";
import {
  Box,
  Button,
  createStyles,
  makeStyles,
  TextField,
  Typography,
} from "@material-ui/core";
import Logo from "../../assets/img/logo-home.png";
import InputAdornment from "@material-ui/core/InputAdornment";
import EmailIcon from "@material-ui/icons/Email";
import Input from "@material-ui/core/Input";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import IconButton from "@material-ui/core/IconButton";
import LockOpenIcon from "@material-ui/icons/LockOpen";
import { useFormik } from "formik";
import * as Yup from "yup"
import api from "../../api";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) =>
  createStyles({
    container: {
      background: "#eeecdb",
      height: "100vh",
      width: "100%",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
    formContainer: {
      width: 360,
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    },
    margin: {
      marginTop: 10,
      marginBottom: 10,
    },
    title: {
      marginTop: 10,
      marginBottom: 10,
      textAlign: "center",
    },
    inputPassword: {
        marginTop: 10,
        marginBottom: 20,
    },
    form: {
      width: "100%",
    },
  })
);



const Login: React.FC = () => {
  const classes = useStyles();
  const history = useHistory();
  const [showPassword, setShowPassword] = useState(false);

  async function login(email: string, password: string) {
    try {
      const response = await api.post('users/auth/sign_in', {
        email,
        password,
      });
      
      localStorage.setItem('session_info', JSON.stringify(response.headers))
      api.defaults.headers.client = response.headers.client;
      api.defaults.headers.uid = response.headers.uid;
      api.defaults.headers['access-token'] = response.headers['access-token'];
      
      history.push('/home');
    }catch(err) {
      console.error(err);
    }

  }

  const {values, handleChange, touched, handleSubmit, errors} = useFormik({
      initialValues: {
          email: '',
          password: ''
      },
      validationSchema: Yup.object({
          email: Yup.string().email("Digite um email válido").required("O email é obrigatório"),
          password: Yup.string().required("A senha é obrigatória"),
      }),
      validateOnChange: false,
      validateOnBlur: false,
      onSubmit: values => login(values.email, values.password),
  })

  return (
    <Box className={classes.container}>
      <Box className={classes.formContainer}>
        <img src={Logo} alt="Logo" />
        <Typography className={classes.title} variant="h6" gutterBottom>
          BEM VINDO AOS <br /> EMPRESAS
        </Typography>
        <Typography className={classes.title}>
          Lorem ipsum dolor sit amet consectetur adipisicing
        </Typography>
            <form className={classes.form} onSubmit={handleSubmit}>
              <TextField
                name="email"
                error={errors.email && touched.email ? true: false}
                className={classes.margin}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <EmailIcon color="primary" />
                    </InputAdornment>
                  ),
                }}
                type="email"
                placeholder="felipe@example.com"
                fullWidth
                onChange={handleChange}
                value={values.email}
              />
              <Input
                error={errors.password && touched.password ? true: false}
                className={classes.inputPassword}
                type={showPassword ? "text" : "password"}
                value={values.password}
                onChange={handleChange}
                name="password"
                inputProps={{
                  'data-testid': 'login.password',
                }}
                fullWidth
                startAdornment={
                  <InputAdornment position="start">
                    <LockOpenIcon color="primary" />
                  </InputAdornment>
                }
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={() => { setShowPassword(!showPassword)}}
                      onMouseDown={() => { setShowPassword(!showPassword)}}
                    >
                      {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                }
              />

              {
                (errors.email || errors.password) && (
                    <Typography color="error" gutterBottom>Credenciais informadas são inválidas, tente novamente</Typography>
                )
              }

              <Button type="submit" color="secondary" variant="contained" fullWidth>
                Entrar
              </Button>
            </form>
      </Box>
    </Box>
  );
};

export default Login;
