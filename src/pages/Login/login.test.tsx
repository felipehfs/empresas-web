import { fireEvent, render, screen, act, waitFor } from '@testing-library/react';
import Login from './index';
import faker from 'faker'
import api from '../../api';
import { MemoryRouter } from 'react-router-dom';

jest.mock('../../api');

const apiMock  = api as jest.Mocked<typeof api>

describe('Login Page', () => {
    test('Should throw an error if the input is blank', async () => {
        render(<Login />);
        const submitButton = await screen.findByText('Entrar');
        
        await act(async() => {
            fireEvent.click(submitButton);
        })

        const alert = await screen.findByText('Credenciais informadas são inválidas, tente novamente');
        await waitFor(() => {
            expect(alert).toBeVisible();
        })
    });

    test('Should make a successfully login', async () => {
        render(<Login />, {
            wrapper: MemoryRouter
        });
        apiMock.post.mockResolvedValue({ data: {}, headers: {}})
        const emailInput = await screen.findByPlaceholderText('felipe@example.com');
        const passwordInput = await screen.findByTestId('login.password');
        const submitButton = await screen.findByText('Entrar');

        await act(async() => {
            fireEvent.change(emailInput, { target: { value: faker.internet.email()}});
            fireEvent.change(passwordInput, { target: { value: '1234'}});
        });
        
        await act(async() => {
            fireEvent.click(submitButton);
        });

        await waitFor(() => {
            expect(apiMock.post).toHaveBeenCalled();
        })
    });
})