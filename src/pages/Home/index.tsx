import React, { useState, useEffect } from "react";
import {
  AppBar,
  Box,
  createStyles,
  IconButton,
  makeStyles,
  Toolbar,
  Typography,
} from "@material-ui/core";
import Logo from "../../assets/img/logo-nav.png";
import { Search } from "@material-ui/icons";
import InputAdornment from "@material-ui/core/InputAdornment";
import Input from "@material-ui/core/Input";
import SearchIcon from "@material-ui/icons/Search";
import CloseIcon from "@material-ui/icons/Close";
import CompanyList from "../../containers/companyList";
import api from "../../api";
import { Company } from "../../models/company";
import { debounce } from "lodash";

const useStyles = makeStyles((theme) =>
  createStyles({
    container: {
      background: "#eeecdb",
      minHeight: "100vh",
      width: "100%",
    },
    navDefault: {
      display: "flex",
      alignItems: "center",
      width: "100%",
    },
    main: {
      paddingTop: 70,
      minHeight: "100vh",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
    listContainer: {
      paddingTop: 100,
      paddingLeft: 16,
      paddingRight: 16,
      minHeight: "100vh",
    },
  })
);

const Home: React.FC = () => {
  const classes = useStyles();
  const [searchMode, setSearchMode] = useState(false);
  const [companies, setCompanies] = useState<Company[]>([]);
  const [query, setQuery] = useState("");

  async function findByNameEnterprise(search: string) {
    const sessionInfo = localStorage.getItem("session_info");
    if (sessionInfo !== null) {
      const session = JSON.parse(sessionInfo);
      api.defaults.headers.client = session.client;
      api.defaults.headers.uid = session.uid;
      api.defaults.headers["access-token"] = session["access-token"];
    }
    const url = search? `enterprises?name=${search}`: `enterprises`;
    const response = await api.get(url);
    setCompanies(response.data.enterprises);
  }

  const debouncedSearch = debounce(findByNameEnterprise, 900);

  function handleOnChangeQuery(
    event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) {
    setQuery(event.target.value);
    debouncedSearch(event.target.value);
  }

  useEffect(() => {
    async function fetchCompany() {
      const sessionInfo = localStorage.getItem("session_info");
      if (sessionInfo !== null) {
        const session = JSON.parse(sessionInfo);
        api.defaults.headers.client = session.client;
        api.defaults.headers.uid = session.uid;
        api.defaults.headers["access-token"] = session["access-token"];
      }
      const response = await api.get(`enterprises`);
      setCompanies(response.data.enterprises);
    }

    fetchCompany();
  }, []);

  return (
    <Box className={classes.container}>
      <AppBar>
        <Toolbar>
          {searchMode ? (
            <nav className={classes.navDefault}>
              <Input
                fullWidth
                value={query}
                onChange={handleOnChangeQuery}
                startAdornment={
                  <InputAdornment position="start">
                    <SearchIcon color="action" />
                  </InputAdornment>
                }
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={() => setSearchMode(false)}
                    >
                      <CloseIcon />
                    </IconButton>
                  </InputAdornment>
                }
              />
            </nav>
          ) : (
            <nav className={classes.navDefault}>
              <div style={{ flex: 1 }}>
                <h1 style={{ textAlign: "center" }}>
                  <img src={Logo} alt="Logotipo" height={25} />
                </h1>
              </div>
              <div>
                <IconButton onClick={() => setSearchMode(true)}>
                  <Search color="action" />
                </IconButton>
              </div>
            </nav>
          )}
        </Toolbar>
      </AppBar>
      {searchMode ? (
        <Box className={classes.listContainer}>
          <CompanyList data={companies} />
        </Box>
      ) : (
        <Box className={classes.main}>
          <Typography>Clique na busca para começar</Typography>
        </Box>
      )}
    </Box>
  );
};

export default Home;
