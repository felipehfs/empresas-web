import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import CompanyDetails from './pages/CompanyDetails';
import Home from './pages/Home';
import Login from './pages/Login';
import PrivateRoute from './components/PrivateRoute'

const Routes : React.FC = props => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Login} />
                <PrivateRoute path="/home" component={Home} />
                <PrivateRoute path="/companies/:id" component={CompanyDetails} />
            </Switch>
        </BrowserRouter>
    )
}

export default Routes;