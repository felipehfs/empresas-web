import React from 'react'
import { makeStyles, Paper, Typography, createStyles } from '@material-ui/core'

interface CompanyItemProps {
    companyLogo: string;
    brand: string;
    description: string;
    address: string;
    onClick: () => void;
}

const useStyles = makeStyles(theme => createStyles({
    card: {
        display: 'flex',
        padding: theme.spacing(2),
        cursor: 'pointer',
    }, 
    logo: {
        marginRight: theme.spacing(8),
    },
    warmGray: {
        color: '#8d8c8c'
    }
}))

const CompanyItem : React.FC<CompanyItemProps> = (props) => {
    const classes = useStyles();
    return (
        <Paper className={classes.card} onClick={props.onClick}>
            <img className={classes.logo} src={props.companyLogo} height="120" alt={`Logo ${props.brand}`}/>
            <main>
                <Typography variant="h6" gutterBottom>{props.brand}</Typography>
                <Typography className={classes.warmGray} variant="subtitle1">{props.description}</Typography>
                <Typography  className={classes.warmGray} variant="caption">{props.address}</Typography>

            </main>
        </Paper>
    )
}

export default CompanyItem
