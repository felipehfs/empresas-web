import React from 'react';
import {Redirect, Route, RouteProps} from 'react-router-dom';

interface PrivateRouteProps extends RouteProps {

}

const PrivateRoute: React.FC<PrivateRouteProps> = (props) => {
  return localStorage.getItem('session_info') ? <Route {...props} /> : <Redirect to="/" />;
};

export default PrivateRoute;
