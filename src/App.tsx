import { createMuiTheme, CssBaseline, ThemeProvider } from '@material-ui/core';
import React from 'react';
import Routes from './Routes';
import ErrorBoundary from './components/ErrorBoundary';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#ee4c77'
    },
    secondary: {
      main: '#57bbbc'
    }
  }
})

function App() {
  return (
    <ThemeProvider theme={theme}>
      <ErrorBoundary>
      <CssBaseline />
      <Routes/>
      </ErrorBoundary>
    </ThemeProvider>
  );
}

export default App;
